<?php
require 'vendor/autoload.php';
// the timezone needs to match NTP
date_default_timezone_set('UTC');

use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;

/**
 * The Class and file name is not following a correct naming convention
 *
 * Class log-migration
 * @author Soroush Atarod
 * Facade class to handle log migration
 * This is a simple script that migrates files to Azure Blob storage
 * it can be easily modified to migrate the files to other sources
 * It's not OO as this is a SRE application test.
 *
 * @see https://pdfs.semanticscholar.org/8e4e/270cfaae8765410ab4292d8b353d2b40f9dc.pdf
 */
class migration
{
    /**
     * @var string Location where the program stores the log
     */
    protected $logDirectory = null;

    /**
     * @var BlobRestProxy
     */
    protected $azureBlobConnection = null;

    /**
     * @var string Name of the Azure Blob container
     */
    protected $blobContainerName = null;

    /**
     * Stores the application config
     *
     * @var array
     */
    protected $config = [];

    /**
     * @todo Add dependency injection
     * @todo should be able to upload the files to various other sources
     * migration constructor.
     */
    public function __construct()
    {
        $this->config = $this->getConfig();
        if (empty(file_exists($this->getLogDirectory()))) {
            throw new Exception('Log Directory not found, please check path');
        }
        $this->migrate();
    }

    /**
     * This is sort of a Circuit Breaker Pattern.
     * As a backup plan, let's say we cannot connect to Azure. We would need to be able
     * to save some space  till Azure Blob service is back.
     *
     * This is why we will compress the files, move it to a different location and delete the original log files
     * on the server itself. 28 mb file compressed turn into 82kb!
     *
     *
     * In a large scale system, services fail, dependencies might break. But having an alternative plan
     * saves us!
     *
     * I have not created a class for it, as this is an SRE application test rather than a programming test
     *
     * @todo Move the Compressed files to a different folder during the backup plan phase so later on we could
     *       migrate it to Azure.
     *
     * @see https://pragprog.com/book/mnee/release-it
     * @see https://docs.microsoft.com/en-us/azure/architecture/patterns/circuit-breaker
     * @see https://12factor.net/
     */
    protected function migrate()
    {
        // @todo make the percentage acceptable via a parameter
        // @todo get the total free disk space of the hard drive not the current volume which the folder is mapped on
        //  This seems that the log files are not written on Azure Files or there might be a limit configured.
        if ($this->getFreeDiskSpacePercentage() < 10) {
            $logFiles = $this->getLogFilesToMigrate();
            if (!empty($logFiles)) {
                try {
                    $this->migrateToAzure($logFiles);
                } catch(Exception $ex) {
                    $this->migrateLocally($logFiles);
                    // Log the exception via NewRelic
                }
            }
        }
    }

    /**
     * @todo check whether a file is being opened by another process using "stat" function.
     * @todo add option to migrate files created X days old.
     *
     *
     * @return array files to be deleted
     */
    protected function getLogFilesToMigrate()
    {
        $logFiles = array_diff(scandir($this->getLogDirectory()), array('..', '.'));
        $yesterday = strtotime('-1 day 23:59:59');
        clearstatcache();
        $logFilesToBeMigrated = [];
        foreach ($logFiles as $file) {
            $file = $this->getLogDirectory()  . $file;
            $fileModifiedTime = filemtime($file);
            if ($fileModifiedTime < $yesterday) {
                $logFilesToBeMigrated[] = $file;
            }
        }

        return $logFilesToBeMigrated;
    }

    /**
     * Handles the migration of the files to Azure
     * @param string $files
     * @throws Exception
     */
    public function migrateToAzure($files)
    {
        try {
            $this->azureBlobConnection = BlobRestProxy::createBlobService($this->config['azure']['connection']);
            $this->setBlobContainer($this->config['azure']['blobContainer']);
            $compressedFile = $this->createZip($files);
            $this->uploadFileToAzureBlob($compressedFile);
            $this->deleteFiles($files);
            if (!empty($compressedFile)) {
                $this->deleteFiles([$compressedFile]);
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }

    }

    /**
     * Handle the migration locally
     * Compress the file, and delete the unused files
     * @param array $files
     */
    public function migrateLocally($files)
    {
        $this->createZip($files);
        $this->deleteFiles($files);
    }

    /**
     * Get free disk space in percentage
     *
     * @return float|int
     */
    public function getFreeDiskSpacePercentage()
    {
        $totalDiskSpace = disk_total_space($this->getLogDirectory());
        $freeDiskSpace = disk_free_space($this->getLogDirectory());
        $percentage = ($freeDiskSpace / $totalDiskSpace) * 100;
        $percentage = round($percentage, 2);
        return $percentage;
    }


    /**
     * Create a ZIP file
     * @param array $srcFiles
     * @return string
     */
    public function createZip($srcFiles = [])
    {
        $yesterdaysDate = strtotime('-1 day 23:59:59');
        // we are adding a random number to the zip name to avoid collision.
        $zipName = date('d-m-Y', $yesterdaysDate)  . '-' . rand(1, 2000).'.zip';
        $zipFile = $this->getZipDirectory() . $zipName ;
        $zip = new ZipArchive();
        $zip->open($zipFile, ZipArchive::CREATE);
        foreach ($srcFiles as $file) {
            $zip->addFile($file, basename($file));
        }
        $zip->close();
        return $zipFile;
    }

    /**
     * Load configs
     *
     * @todo make it Singleton
     * @return array
     */
    protected function getConfig()
    {
        // can load config from anywhere later on
        $config = require_once 'config.php';
        return $config;
    }


    /**
     * Get Temporary Zip Directory
     * @return string
     */
    public function getZipDirectory()
    {
        $zipDir =  $this->config['storage']['zip'];
        if (false == file_exists($zipDir)) {
            mkdir($zipDir);
        }

        return $zipDir;
    }

    /**
     * Get the location of the log
     * @return string
     */
    public function getLogDirectory()
    {
        $path = $this->config['storage']['log'];
        return $path;
    }

    /**
     * Uploads File to Azure Blob
     * @param string $file Path to the file
     */
    protected function uploadFileToAzureBlob($file)
    {
        $content = fopen($file, "r");
        $fileAttributes = pathinfo($file);
        try {
            $this->azureBlobConnection->createBlockBlob($this->blobContainerName, $fileAttributes['basename'], $content);
        } catch (ServiceException $e) {
            $code = $e->getCode();
            $error_message = $e->getMessage();
            echo $code . ": " . $error_message . PHP_EOL;
        }
    }

    /**
     * Delete files
     *
     * @param array $files
     */
    public function deleteFiles($files = [])
    {
        array_map('unlink', $files);
    }

    /**
     * Formats the size in units
     *
     * @param string $bytes
     * @return string
     *
     * @see https://stackoverflow.com/questions/5501427/php-filesize-mb-kb-conversion
     */
    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2);
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    /**
     * Send notification about something disaster
     * @param string $message
     */
    public function dispatchNotifications($message)
    {
        // send notification via Azure Service Bus,  (producer/consumer) protocol, NewRelic
        // alert that migration of files has failed and it is compressing the file locally
        // always expect failures but have a strategy in place
    }

    /**
     * @param string $containerName
     *
     * @see https://github.com/Azure/azure-storage-php/blob/master/samples/BlobSamples.php
     */
    public function setBlobContainer($containerName)
    {
        // defensive {assume the container is not yet created! we don't want failures in SRE }
        $containers  = $this->azureBlobConnection->listContainers()->getContainers();
        $containerExist = false;

        foreach ($containers as $container) {
            if ($container->getName() == $containerName) {
                $containerExist = true;
            }
        }

        if (false == $containerExist) {
            // check if the container exists if its not there, let's create it!
            // always predict things don't work as expected
            $createContainerOptions = new CreateContainerOptions();
            try {
                $this->azureBlobConnection->createContainer($containerName, $createContainerOptions);
            } catch (ServiceException $e) {
                $code = $e->getCode();
                $error_message = $e->getMessage();
                echo $code.": ".$error_message.PHP_EOL;
            }
        }

        $this->blobContainerName = $containerName;

    }
}

$php = new migration();